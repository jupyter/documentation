# Instructions for Instructors

## Update Notebooks included in the profile

In case you provided a URL to a Git repository while requesting your profile, you can update the contents of the repository at any time using standard Git workflows.

The contents of your repositry will be synchronized during each start of the profile with the local copy of the user in his RWTHjupyter home directory. We use [nbgitpuller](https://github.com/jupyterhub/nbgitpuller) to perform this synchronization.

Please note that users need to completely restart their Jupyter service before changes become available in their Jupyter instance.
To force a restart, perform the following steps:

1. Stop your current server by clicking on "Stop my server" on the [JupyterHub home page](https://jupyter.rwth-aachen.de/hub/home).
2. Wait a minute, then reload the page
3. Start the server again by selecting a profile.

Alternatively, you can force a synchronization while the Jupyter server is running:

1. Access your (running) Jupyter server at: [https://jupyter.rwth-aachen.de/](https://jupyter.rwth-aachen.de).
2. Open a terminal: File -> New -> Terminal
3. Run the following command: `bash /scripts/start.sh`

## Rebuild Docker image for your profile

All changes to the repository which affect the runtime environemnt (installed jupyter kernel, Conda's `environment.yml`, Pip's `requirements.txt` ) require a rebuild of the underlying Docker image of your profile.

Such a rebuild needs to triggerd manually by the profile manager:

1. Check if you have the manager role for your profile by visiting: https://jupyter.rwth-aachen.de/services/whoami/
  - Ensure that you are member of a group named: `manager-{profile_slug}`.
2. Visit the following service to trigger a rebuild: https://jupyter.rwth-aachen.de/services/profile/ 
3. Select your profile from the drop-down
4. Press the button "Trigger new build of Docker image"

