# Terms of Use

## §1 Subject of the rules of use

The following rules of use describe the general conditions for the use of the pilot project _RWTHjupyter_ for its higher education (especially teaching students). RWTHjupyter is a web-based interactive computational environment for creating Jupyter notebook documents.

These rules are agreed upon for the use of the _RWTHjupyter_ pilot project.

In the following, for a better understanding, the users are uniformly referred to as "users".
Users can be divided into two subgroups - "teachers" and "students".

## §2 Conditions of use

1. The present rules refer exclusively to the use of the _RWTHjupyter_.
2. The use of the _RWTHjupyter_ requires an enrollment or employment at the RWTH, the use of a browser and the use of a client software.
3. The use of the _RWTHjupyter_ is restricted to teaching related activities. This includes theses, student projects or autodidactic activities.

## §3 Costs

Use of the _RWTHjupyter_ is free of charge; there is no legal claim to registration and use.

## §4 Duties of care

The user is obliged to treat his access data confidentially and to protect it from access by third parties. The user must therefore take all necessary measures to ensure the security and confidentiality of the access data and passwords generated by him. In case of possible misuse of his access data, the user must inform the RWTH immediately. He is also responsible for the consequences of such misuse.

The user may not take any measures or use any software that could interfere with the functioning of the _RWTHjupyter_ service or otherwise interfere with its availability.

In particular, the amount of data, the number of objects (number of files) and the number of simultaneous connections are subject to the specifications made previously or the standard parameters of the _RWTHjupyter_.

Deviating uses of the provided software (e.g. the use of encryption technologies) are the sole responsibility of the user.

In case of violation by the user, the access authorization of the user will be blocked.

Furthermore, the user must always observe and comply with the instructions given.

## §5 Rights and duties of the user

1. By using the _RWTHjupyter_, the user grants the RWTH the right to store the data and work results he/she has stored in the _RWTHjupyter_ and to view them under the following conditions.
2. The data and work results stored by the user in the _RWTHjupyter_ may be inspected by the RWTHjupyter admins, if

   - the employment relationship between the user and the RWTH is terminated
   - or the user is not available for more than 1 week to disclose the stored data and work results himself;
   - or the user violates the terms and conditions of use and as a consequence is blocked from using the service.

3. Rights of use and copyrights to the stored information remain unaffected.
4. Access to the cluster may be suspended due to excessive use, crypto miners or other types of misuse.

## §6 User Lifecycle

1. Users must log in to RWTHjupyter again at the latest 12 months after their last login. During this process a check is made to see whether the access conditions for using the service are still met.
2. If the access conditions (§2) after this period are not met, the access to the cluster will be disabled.
3. In case the last user activity was more than 12 months ago, the cluster access will be disabled and the user account will be deaktivated. The user will be notified via mail about a pending deletion of account 6 months after the deactivation.
4. In case the last user activity was more than 18 months ago, the user account will be finally deleted including all stored data. Shared folders are only deleted after all user accounts with access to the share have been expired.

Users who have their account deactivated due to unmet access conditions can contact the administrators to regain access to their user data within the 18 month deletion period.

![User Lifecycle Diagram](./images/UserLifecycle.svg)

## §7 Liability

The RWTH has unlimited liability in cases of intent or gross negligence, for injury to life, limb or health and according to the regulations of the Product Liability Act.

In case of slightly negligent violation of an obligation that is essential for the achievement of the purpose of the contract (cardinal obligation), the liability of the RWTH is limited to the amount of damage that is foreseeable and typical for the type of business in question.

A further liability of the RWTH does not exist.

The aforementioned limitation of liability also applies to the personal liability of employees, representatives and organs of the RWTH.

## §8 Availability of the RWTHjupyter service

A permanent, trouble-free and/or unlimited availability of the service cannot be guaranteed or offered. Especially maintenance work or security aspects as well as force majeure and events beyond the control of the RWTH can lead to disturbances or a temporary suspension of the service.

## §9 Data protection

The collection, use and application of personal data is carried out in accordance with the relevant data protection regulations. In particular, no personal data is passed on to third parties without authorisation.

When logging in to the JuptyerHub cluster, the following information about the user's person is transferred by the RWTH's identity management system and stored in the user database of the service:

- E-mail addresses
- rwthSystemIDs (Beschränkt auf die lsm-id der Personen aus Moodle)
- [eduPersonTargetedID](https://doku.tid.dfn.de/de:common_attributes#a11)
- [eduPersonScopedAffiliation](https://doku.tid.dfn.de/de:common_attributes#a09)

In addition, the following user identifiable information will be stored for a period of 4 weeks and linked to the users account:

- Time stamps of singleuser container spawns and terminations
- Timestamp of last user activity on the cluster

All user data will be deleted after a total of 18 months of user inactivity (see §6 User Lifecycle)

## §10 Miscellaneous

1. Changes and amendments to this contract must be made in writing. This also applies to the amendment or cancellation of this clause.
2. General terms and conditions of the user shall not apply.
3. German law shall apply to this contract to the exclusion of the United Nations Convention on Contracts for the International Sale of Goods of 11.4.1980 (UN Sales Convention).
4. Place of performance is Aachen. Exclusive place of jurisdiction is Aachen, provided that each party is a merchant or legal entity under public law or has no general place of jurisdiction in Germany.
5. Should individual provisions of this contract be invalid, this shall not affect the validity of the remaining provisions. The parties to the contract shall endeavour to find a valid provision in place of the invalid provision, which comes closest to the economic meaning of the invalid provision.
