# Launch Profiles and Notebooks via Links

This page shows a few examples of permanent links for accessing profiles or notebooks in the Jupyter cluster.
They can be used for your README.md, Websites or Moodle.

## Link creation wizard

Feel free to use the [link creation wizard](https://jupyter.rwth-aachen.de/services/link/) to create links the the schemes described below.

## Spawn and open Notebook

Opens a specific notebook using the default profile (Python) or your running Jupyter server.

**Schema:** `https://jupyter.rwth-aachen.de/user-redirect/lab/tree/{{ notebook_path }}`  
**Example:** https://jupyter.rwth-aachen.de/user-redirect/lab/tree/gdet3/GDET3%20Faltung%20GUI.ipynb

#### Arguments

- `notebook_path` and URL encoded path to the Notebook which should be opened. Please not that this path must be [URL encoded](https://www.w3schools.com/tags/ref_urlencode.ASP).

## Spawn with a specific profile

This link to spawn a specific profile.

**Note:** If a server is already running with a different profile, no action will be taken.

**Schema:** `https://jupyter.rwth-aachen.de/hub/spawn?profile={{ profile_slug }}`  
**Example:** https://jupyter.rwth-aachen.de/hub/spawn?profile=gdet3

#### Arguments

- `profile_slug` a short alpha numeric identifier for the profile. Please take a look at the [profile list](Profiles.md) for available profiles.

## Spawn with a specific profile and open a notebook

This version combines the first two types of links:

**Schema:** `https://jupyter.rwth-aachen.de/hub/spawn?profile={{ profile_slug }}&next=/user-redirect/lab/tree/{{ notebook_path }}`  
**Example:** https://jupyter.rwth-aachen.de/hub/spawn?profile=gdet3&next=/user-redirect/lab/tree/gdet3/GDET3%20Faltung%20GUI.ipynb

### Specify username and server name

Using this slighly extended version, a named server for a specific user can be spawned.
Please note that user impersonalization is only available for administrators.

A normal user can start up to 10 named servers. E.g. for using several profiles in parallel

**Schema:** `https://jupyter.rwth-aachen.de/hub/spawn/{{ user_name }}/{{ server_name }}?profile={{ profile_slug }}`  
**Example:** https://jupyter.rwth-aachen.de/hub/spawn/vzi3jsam/my_et3_server?profile=gdet3

#### Arguments

- `server_name`  is a alpha numeric identifier for the named server

## Badge

You can use the following Markdown or HTML snippets to embed a badge into your `README.md` files or Moodle activities for launching a Notebook:

### Example

[![](https://jupyter.pages.rwth-aachen.de/documentation/images/badge-launch-rwth-jupyter.svg)](https://jupyter.rwth-aachen.de/hub/spawn?profile=pti&next=/user-redirect/lab/tree/pti/index.ipynb)
[![](https://mybinder.org/static/images/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.rwth-aachen.de%2FIENT%2Fpti/master?urlpath=lab/tree/index.ipynb)

### Markdown

```markdown
[![](https://jupyter.pages.rwth-aachen.de/documentation/images/badge-launch-rwth-jupyter.svg)](https://jupyter.rwth-aachen.de/hub/spawn?profile=pti&next=/user-redirect/lab/tree/pti/index.ipynb)
[![](https://mybinder.org/static/images/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.rwth-aachen.de%2FIENT%2Fpti/master?urlpath=lab/tree/index.ipynb)
```

### HTML

```html
<a href="https://jupyter.rwth-aachen.de/hub/spawn?profile=pti&next=/user-redirect/lab/tree/pti/index.ipynb"><img src="https://jupyter.pages.rwth-aachen.de/documentation/images/badge-launch-rwth-jupyter.svg" /></a>
<a href="https://mybinder.org/v2/git/https%3A%2F%2Fgit.rwth-aachen.de%2FIENT%2Fpti/master?urlpath=lab/tree/index.ipynb"><img src="https://mybinder.org/static/images/badge_logo.svg" /></a>
```
