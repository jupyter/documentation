# Share Notebooks with other users

RWTHjupyter support several mechanisms to share notebooks between users.

## By manual exchange of `.pynb` files

The easiest way of sharing notebooks with other users is by downloading/uploading the `.ipynb` files via the menu in JupyterLab.
This can then be easily emailed to colleagues who can open it back in JupyterLab (just drag and drop the file into file browser of JupyterLab).

## By using a shared folder

In addition, each user has access to a `shared/` folder in their home directory by default.
This folder is shared between all users and can therefore be used well for sharing notebooks.

**Attention:** Please note that all users have access and can also customize or delete their notebooks there as they see fit.

We are currently still working on a possible to also allow private-shared folders. Similar to what you might already know from Sciebo.
But we can't make a concrete statement about the availability yet.

## By granting the other user full access to your Jupyter server

Users can share access to their running Jupyter server by sharing a link with other users.

**Attention:** Only share the link with trusted friends, colleagues and partners as it grants the receiving user full access to your Jupyter account!!!

Copy the following code snippet into your notebook to craft a shareable link.
Please note that you can provide a path (optional) and an expiration date for the link.

```python
import rwth_nb.misc.share as share
import rwth_nb.misc.notebook as nb

from IPython.core.display import HTML

url = share.get_shareable_url(path=nb.get_current_path(), note='access for my colleage at IKS', expires_in=24*60*60)

HTML(f'<a href="{url}">{url}</a>')
```
