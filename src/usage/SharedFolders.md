# Shared folders

By default every user sees three special folders in his home directory:

- `~/materials`
- `~/datasets`
- `~/shared`

These folders are addition volume mounts and share the same content between all users.
These folders are also read-only with the exception of `~/shared` which can be used as a global scratch pad to exchange files between users on the cluster.

## `materials`

This folder contains a selection of Jupyter notebooks which can be used to explore the capabilities of Jupyter Lab.
They are also well suited as a starting point for new lectures.

The example materials are managed by a dedicated Git repository.
Pull requests to add new materials are welcome:

https://git-ce.rwth-aachen.de/jupyter/example-materials

## `datasets`

This folder contains a collection of well-known machine learning datasets.
Due to their size, they are shared between all users.

Feel free to contact us, if you wish to add you examples or datasets.

## `shared`

This folder can be used as a global scratch pad to exchange files between all users of the cluster.
All users can read, write, modify and delete files from this folder.
No backups are made!

## Group Shares / Sciebo Integration

We are currently working on a solution to allow the restriction of shared folders to a smaller user group.
Also possibly the integration of external Sciebo shares.
Please feel free to [contact](Contact.md) us if you are interested in this feature.
