# Instructions

## Links

- [JupyterLab Documentation](https://jupyterlab.readthedocs.io/en/stable/)
- [Jupyter Documentation](https://jupyter.org/documentation)
- [Jupyter Discussion Forum](https://discourse.jupyter.org/)

## Open a Jupyter environment

1. Please visit the Webpage of the Cluster at: https://jupyter.rwth-aachen.de
2. Click on "Sign in with Shibboleth" and use your RWTH TIM-ID to login
3. Choose a profile from the list and click on "Start" at the bottom of the page
  - Alternatively, you can use the search function to narrow down the selection of profiles
4. Wait until your personal Jupyter environment has been spawned. This can take between 5-60 seconds.

