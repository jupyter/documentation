# Frequently Asked Questions (FAQ)

### Are there any resource quotas/limits enforced on singleuser containers on RWTHjupyter?

Yes, we currently provide each user container with a maximum of 64 GiB RAM, 32 CPU cores and a 4 GiB of persistent storage space for your home directory.

### Can I use RWTHjupyter for my thesis/research/hobby project?

Please consult the [Terms of Use](TermsOfUse.md) for details.

### Can I use my own Jupyter environment or install additional packages / kernels?

This is only possible to a limited extend.
We encourage all instructors and professors to [apply for a customized profile for their course](instructors/NewProfiles.md).

Students who which to use _RWTHjupyter_ outside of their courses, we recommend one of the [generic kernel profiles](Profiles.md).
These generic profiles can by customized by installation additional packages via `pip` and `conda`.
By default, added packages are not persistent and only available until the next spawn of your Jupyter container.
You can work around this, by installing packages into your home directory:

```bash
pip install --user pandas
```

You can also add this line into a Jupyter Notebook cell by prefixing it with an exclamation mark:

```python
!pip install --user pandas
```

It is currently not possible to load custom conda environemnts.

### How can I rebuild the Docker image which used for my profile?

Please visit the following site and select your profile to trigger a new build of the Docker image which is used to spawn single user containers:

https://jupyter.rwth-aachen.de/service/profile/

**Note:** You need to hold the manager role of the profile which you want to rebuild. Please [contact us](Contact.md), if you dont have the role yet.

### What is the purpose of the `shared`, `materials` and `dataset` directories in my home directory

Please have a look at the following dedicated page: [Shared Folders](usage/SharedFolders)

### How can I share a notebook with colleagues, friends, partners or my instructor?

Please have a look at the following dedicated page: [Links](usage/SharedNotebooks.md)

### Can RWTHjupyter be used by external partners?

Yes. Please use the [RWTH IdM Partner Manager](https://help.itc.rwth-aachen.de/service/rhb2fhkpjhb7/article/vey2yicky5b6) to sponsor a RWTH partner account.
Partner accounts can access RWTHjupyter but might have reduced privileges or priorities.

### How can I create a perma-link to a open a specific profile and/or Jupyter notebook?

Please have a look at the following dedicated page: [Links](usage/Links.md)

### This is cool! How can I contribute or improve RWTHjupyter?

We host most of our code, configuration and more on the [RWTH GitLab instance](https://git.rwth-aachen.de/jupyter/).

Please feel free to contribute by submitting merge requests.

We are also looking for HiWi's to support us in improving this service. Feel free to [get in touch with us](Contact.md).

### Who is behind RWTHjupyter?

The _RWTHjupyter_ infrastructure was created in collaboration between the Institute for [Automation of Complex Power Systems (ACS)](https://acs.eonerc.rwth-aachen.de) and the [IT Center of RWTH Aachen University](https://itc.rwth-aachen.de).

### I've lost my changes to my notebooks. Changes to the courses Notebooks are not synchronized properly.

We use a Jupyter extension called [nbgitpuller](https://jupyterhub.github.io/nbgitpuller/) to sync Jupyter Notebooks.
nbgitpuller uses an "automatic merging behaviour" to sync changes between your local home directory and the upstream Git repo.
Please consult the nbgitpuller documentation for details about this [merging behaviour](https://jupyterhub.github.io/nbgitpuller/topic/automatic-merging.html).

### How can use GPUs on RWTHjupyter

Currently, GPUs are only available to selected courses. Please [contact us](Contact.md) if you wish to use GPU resources.
