# Legal

## Imprint

Please refer to the [imprint of the IT Center of RWTH Aachen University](https://www.itc.rwth-aachen.de/cms/IT-Center/Footer/Service/~epvv/Impressum/?lidx=1).

## Data Privacy

Please see [Terms of Use](TermsOfUse.md) § 9 until further notice. 
