# Support

Support for _RWTHjupyter_ is provided on a best-effort basis by the RWTH Jupyter user-group.
Please note that it is not yet provided as an official service by the RWTH IT Center.

You can contact the group via its mailing list: [jupyter@lists.rwth-aachen.de](mailto:jupyter@lists.rwth-aachen.de).
A subscription to this list is possible via the following link: https://lists.rwth-aachen.de/postorius/lists/jupyter.lists.rwth-aachen.de/

You can also contact the administrators of the cluster via a dedicated closed list: [jupyter-admin@lists.rwth-aachen.de](mailto:jupyter-admin@lists.rwth-aachen.de).
