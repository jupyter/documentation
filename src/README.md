# RWTHjupyter

![Build Status](https://git.rwth-aachen.de/jupyter/documentation/badges/master/pipeline.svg)

<img src="./images/jupyter.png" alt="Jupyter" style="text-align: center" width=500 />

---

##  Service Description

With _RWTHjupyter_ the IT Center provides in collaboration with the Institute for Automation of Complex systems all users with access to an interactive computing platform.

## Costs

The cluster has been initially funded by the Projekt _Digitale Lehr-/Lerninfrastrukturen DH NRW_ in 2019 and is thereby provided cost-free to all students and employees of the RWTH.

## Hardware

The _RWTHjupyter_ cluster consists of 7 Dell PowerEdge R740xd servers with the following configuration:

7x Dell PowerEdge 740XD:

- Dual Socket Systems: 2x 16C / 32T Xeon Gold 5218 2,3 Ghz
- Redundant Dual 10 GigE links
- 768 GB DDR4 RAM / node (5.376 GB total)
- 100 TB SSD Storage in [Ceph](https://ceph.io/)

Additionally, 1 of the nodes is equipped with:

- 2x NVIDIA Tesla T4 GPGPUs

## Software Configuration

_RWTHjupyter_ runs on a [highly-available Kubernetes cluster](http://kubernetes.io) using the [Zero 2 JupyterHub](https://zero-to-jupyterhub.readthedocs.io/en/latest/) project.

## News

### 2020-04-21: Go-live of RWTHjupyter test phase

### 2020-02-28: Kick-off meeting of the RWTH Jupyter Working group

### 2020-01-24: The first nodes of the cluster have been provisioning at ITC Wendlinge

### 2019-11: A GPU extension for the cluster has been specified and ordered

### 2019-10: The cluster has been specified and ordered

### 2019-06 The RWTHjupyter proposal has been funded by the Project "[Digitale Lehr-/Lerninfrastrukturen DH NRW](https://www.dh.nrw/foerderung)"

<img alt="EON Energy Research Center" src="./images/eonerc.png" width=400 />
<img alt="IT Center" src="./images/itc.png" width=400 />
