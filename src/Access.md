# Access

_RWTHjupyter_ is accessable world-wide over the internet via [jupyter.rwth-aachen.de](https://jupyter.rwth-aachen.de).

Access to _RWTHjupyter_ is granted via the universities Identity Management using a TIM-ID.
By default all employes and students are given access to the cluster without any additional registration.

The use of this cluster is limited to teaching related activities.
Please consult the [Terms of Use](TermsOfUse.md) for details.
